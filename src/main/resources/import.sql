/*insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Greta Pizza');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Hopital');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Voyage');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Coiffeur');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-02-01','2023-03-15','Coiffeur');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Cinema');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Greta');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','Film');
insert into projet (date_debut, date_fin, nom) VALUES ('2023-01-01','2023-03-15','E-commerce');
*/

insert into rentree (branche,annee,code) VALUES ('CDA', '2021','CDA 2021 rouge');
insert into rentree (branche,annee,code) VALUES ('CDA', '2022', 'CDA 2022 vert');






-- Insertion des équipes
INSERT INTO equipe (id, nom, id_projet) VALUES (1, 'Equipe Alpha', 1);
INSERT INTO equipe (id, nom, id_projet) VALUES (2, 'Equipe Beta', 2);
INSERT INTO equipe (id, nom, id_projet) VALUES (3, 'Equipe Gamma', 3);
-- Ajouter plus d'équipes jusqu'à 10...

-- Insertion des projets
INSERT INTO projet (id, nom, date_debut, date_fin, id_professeur, id_rentree) VALUES (1, 'Projet A', '2023-01-01', '2023-06-30', 1, 1);
INSERT INTO projet (id, nom, date_debut, date_fin, id_professeur, id_rentree) VALUES (2, 'Projet B', '2023-02-01', '2023-07-30', 2, 1);
INSERT INTO projet (id, nom, date_debut, date_fin, id_professeur, id_rentree) VALUES (3, 'Projet C', '2023-03-01', '2023-08-30', 3, 1);
INSERT INTO projet (id, nom, date_debut, date_fin, id_professeur, id_rentree) VALUES (4, 'Projet D', '2023-04-01', '2023-09-30', 4, 1);
-- Ajouter plus de projets jusqu'à 10...

-- Insertion des utilisateurs étudiants
-- Équipe 1
INSERT INTO utilisateur (id, nom, prenom, email, mdp, role, type_utilisateur, id_equipe) VALUES (1, 'Doe', 'John', 'john.doe@example.com', 'hashed_password', 'ROLE_ETUDIANT', 'etudiant', 1);
-- Équipe 2
INSERT INTO utilisateur (id, nom, prenom, email, mdp, role, type_utilisateur, id_equipe) VALUES (2, 'Roe', 'Jane', 'jane.roe@example.com', 'hashed_password', 'ROLE_ETUDIANT', 'etudiant', 2);
-- Équipe 3
INSERT INTO utilisateur (id, nom, prenom, email, mdp, role, type_utilisateur, id_equipe) VALUES (3, 'Dane', 'Joe', 'joe.dane@example.com', 'hashed_password', 'ROLE_ETUDIANT', 'etudiant', 3);
-- Ajouter plus d'étudiants à des équipes différentes, en respectant la contrainte 1 à 3 étudiants par équipe

-- (Répéter l'insertion des utilisateurs pour chaque équipe, en changeant les valeurs des champs id, nom, prenom, email et id_equipe)




insert into utilisateur (type_utilisateur,mdp,email,nom,prenom) VALUES  ('ADMIN','12345678','vanille@hotmailfr','$2y$10$9KOlZ9iLpT2Ce0TCzgkwLOCwfTfaSBQ2IxK7IUtgF5Iv4tZ.kp5Gy','timo','pierre');
INSERT INTO utilisateur (type_utilisateur, confirm_mdp, email, mdp, nom, prenom, role, id_equipe) VALUES ('admin', '12345678', 'Na.bi@gmail.fr', '$2y$10$N39LN5by1rPRqtiof/Ym6.3ZK7ZC6Ss8hlh1OGH6QNy8P4VLvxU3m', 'Na', 'bi', NULL, NULL);


INSERT INTO utilisateur (type_utilisateur, confirm_mdp, email, mdp, nom, prenom, role, id_equipe) VALUES ('etudiant',  '12345678', 'candidat@gmail.fr', '$2y$10$QgVUgBINfLDxyg5HbmDgBeNxXbTvsVcyJUteaTdLnE0uGF5b0HC2e', 'Candidat', 'Candidat', NULL, NULL);
INSERT INTO utilisateur (type_utilisateur, confirm_mdp, email, mdp, nom, prenom, role, id_equipe) VALUES ('etudiant2',  '12345678', 'candidat2@gmail.fr', '$2y$10$wQWDyb/QzG2BfO3cwZPYpOLQk.1Mo7D2jjJbpy18U1uWzMLjQUZti', 'Candidat1', 'Candidat1', NULL, NULL);