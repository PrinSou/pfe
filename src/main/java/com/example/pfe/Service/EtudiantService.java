package com.example.pfe.Service;
import com.example.pfe.DAO.EtudiantDAO;
import com.example.pfe.DAO.ProjetDAO;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Etudiant;
import com.example.pfe.Model.Projet;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service

public class EtudiantService {

    private EtudiantDAO etudiantDAO;
    private PasswordEncoder encoder;

    @Autowired
    private ProjetDAO projetDAO;

    public EtudiantService(EtudiantDAO etudiantDAO, PasswordEncoder encoder, ProjetDAO projetDAO) {
        this.etudiantDAO = etudiantDAO;
        this.encoder = encoder;
        this.projetDAO = projetDAO;
    }



    public List<Etudiant> getAll() {
        return etudiantDAO.findAll();
    }

    public Etudiant getEtudiant(Long id) {
        Etudiant etudiant = etudiantDAO.findById(id).orElseThrow();
        return etudiant;
    }

    public Etudiant getEtudiantById(Long id) {
        return etudiantDAO.findById(id).orElse(null);
    }

    public Page<Etudiant> etudiantsPage(String key, Pageable page) {

        return this.etudiantDAO.findByNomContainingIgnoreCase(key, page);
    }


    /*public  Candidat save(CandidatDTO candidatDTO){
      Candidat candidat=this.toCandidat(candidatDTO);


      return candidatDAO.save(candidat);
  }*/
   /* public  Candidat saveCandidat(Candidat candidat){ return candidatDAO.save(candidat);
    }*/

    public Etudiant inscrireEtudiant(UtilisateurDTO dto) {
        Etudiant user = this.toEtudiant(dto);
        // user.setEmail(user.getEmail());
        user.setMdp(encoder.encode(dto.getMdp()));
        // System.out.println(user.getMdp());
        user.setRole("ROLE_Etudiant");

        return etudiantDAO.save(user);
    }

    //Dans la méthode toCandidat(), ajoutez une condition pour vérifier
// si la propriété equipeList de l'objet UtilisateurDTO est non nulle. Si c'est le cas,
// attribuez la valeur de la propriété equipeList de l'objet UtilisateurDTO
// à la propriété equipeList de l'objet Candidat.
    public Etudiant toEtudiant(UtilisateurDTO utilisateurDTO) {
        Etudiant etudiant;
        if (utilisateurDTO.getId() != null) {
            etudiant = etudiantDAO.findById(utilisateurDTO.getId()).orElseThrow();
        } else {
            etudiant = new Etudiant(null, "ti", "ines", "ti.ines@gmail.com", "123","123");
        }
        etudiant.setNom(utilisateurDTO.getNom());
        etudiant.setPrenom(utilisateurDTO.getPrenom());
        etudiant.setEmail(utilisateurDTO.getEmail());
        etudiant.setMdp(utilisateurDTO.getMdp());
        /*if (utilisateurDTO.getEquipeList() != null) {
            etudiant.setEquipe((Equipe) utilisateurDTO.getEquipeList());
        }*/
        return etudiant;
    }

    //Dans la méthode toCandidatDTO(), ajoutez une condition pour vérifier
    // si la propriété equipeList de l'objet Candidat est non nulle. Si c'est le cas,
    // attribuez la valeur de la propriété equipeList de l'objet Candidat
    // à la propriété equipeList de l'objet UtilisateurDTO.
    public UtilisateurDTO toEtudiantDTO(Etudiant etudiant) {
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();

        utilisateurDTO.setId(etudiant.getId());
        utilisateurDTO.setNom(etudiant.getNom());
        utilisateurDTO.setPrenom(etudiant.getPrenom());
        utilisateurDTO.setEmail(etudiant.getEmail());
        utilisateurDTO.setMdp(etudiant.getMdp());

      /*  // Update equipeList
        if (etudiant.getEquipe() != null) {
            utilisateurDTO.setEquipeList((List<Equipe>) etudiant.getEquipe());
        }*/

        return utilisateurDTO;
    }


    public void deleteEtudiantById(Long id) {
        // Recherche de l'étudiant par son identifiant.
        Optional<Etudiant> etudiant = etudiantDAO.findById(id);

        // Vérification de la présence de l'étudiant.
        if (etudiant.isPresent()) {
            // Si l'étudiant est trouvé, procéder à sa suppression.
            etudiantDAO.delete(etudiant.get());
        } else {
            // Si l'étudiant n'existe pas, lever une exception.
            // Vous pouvez aussi choisir de gérer cette situation différemment,
            // comme enregistrer un message d'erreur dans un journal (log).
            throw new EntityNotFoundException("Étudiant avec l'identifiant " + id + " n'a pas été trouvé.");
        }
    }

    public Etudiant findByUsername(String username) {
        // Recherche de l'étudiant par son adresse e-mail en utilisant Optional
        Optional<Etudiant> optionalEtudiant = etudiantDAO.findByEmail(username);
        // Retourner l'étudiant s'il est présent, sinon retourner null
        return optionalEtudiant.orElse(null);
    }








}