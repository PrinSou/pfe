
package com.example.pfe.Service;

import com.example.pfe.DAO.ProfesseurDAO;



import com.example.pfe.DTO.UtilisateurDTO;

import com.example.pfe.Model.Professeur;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProfesseurService {


    private ProfesseurDAO professeurDAO;

   private PasswordEncoder encoder;


    @Autowired
    public ProfesseurService(ProfesseurDAO professeurDAO, PasswordEncoder encoder
                         ) {
        this.professeurDAO = professeurDAO;
        this.encoder = encoder;

    }


    public List<Professeur> getAll() {
        return professeurDAO.findAll();
    }

    public Professeur getProfesseur(Long id) {
        Professeur professeur = professeurDAO.findById(id).orElseThrow();
        return professeur;
    }

    public Professeur getProfesseurById(Long id) {
        return professeurDAO.findById(id).orElse(null);
    }

    public Page<Professeur> professeurPage(String key, Pageable page) {
        return this.professeurDAO.findByNomContainsIgnoreCase(key, page);
    }

    public Professeur save(UtilisateurDTO utilisateurDTO) {
        Professeur professeur = this.toProfesseur(new UtilisateurDTO());
        return professeurDAO.save(professeur);
    }

    public Professeur saveProfesseur(Professeur professeur) {
        return professeurDAO.save(professeur);
    }

    public Professeur inscrireProfesseur(UtilisateurDTO dto) {
        Professeur user = this.toProfesseur(dto);
        user.setEmail(user.getEmail());
       // user.setMdp(encoder.encode(user.getMdp()));
        System.out.println(user.getMdp());
        return professeurDAO.save(user);
    }


    public Professeur toProfesseur(@Valid UtilisateurDTO utilisateurDTO) {
        Professeur professeur = new Professeur();

        professeur.setId(utilisateurDTO.getId());
        professeur.setNom(utilisateurDTO.getNom());
        professeur.setPrenom(utilisateurDTO.getPrenom());
        professeur.setEmail(utilisateurDTO.getEmail());
        professeur.setMdp(utilisateurDTO.getMdp());
      professeur.setRole("Tuteur"); // Set role to Tuteur

        return professeur;
    }

    public UtilisateurDTO toProfesseurDTO(Professeur professeur) {
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();

        utilisateurDTO.setId(professeur.getId());
        utilisateurDTO.setNom(professeur.getNom());
        utilisateurDTO.setPrenom(professeur.getPrenom());
        utilisateurDTO.setEmail(professeur.getEmail());
      utilisateurDTO.setRole(professeur.getRole()); // Inclure le champ role

        return utilisateurDTO;
    }

    public void deleteProfesseurById(Long id) {
        professeurDAO.deleteById(id);
    }
}
