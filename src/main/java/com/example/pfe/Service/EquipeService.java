package com.example.pfe.Service;


import com.example.pfe.DAO.EquipeDAO;
import com.example.pfe.DTO.EquipeDTO;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Etudiant;
import com.example.pfe.Model.Projet;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service


public class EquipeService {

    private EquipeDAO equipeDAO;

    private ProjetService projetService;



    @Autowired

    public EquipeService(EquipeDAO equipeDAO, ProjetService projetService) {
        this.equipeDAO = equipeDAO; this.projetService = projetService;
    }

    public List<Equipe>getAllEquipes(){
        return  equipeDAO.findAll();
    }

    public List<Equipe>getEquipeByNom(String nom){return equipeDAO.getEquipeByNom(nom);}

    public Equipe getEquipeById(Long id){
        return equipeDAO.findById(id).orElse(null);
    }

    public Page<Equipe>pageEquipes(String key, Pageable page){
        return this.equipeDAO.findByNomContainsIgnoreCase(key, page);}

   public  Equipe save(EquipeDTO equipeDTO){
        Equipe equipe=this.toEquipe(equipeDTO);
        return equipeDAO.save(equipe);
    }
   /** public Equipe save(Equipe equipe){
        return equipeDAO.save(equipe);
    }**/

    public Equipe toEquipe(EquipeDTO equipeDTO) {
        Equipe equipe;
        if (equipeDTO.getId() != null) {
            equipe = equipeDAO.findById(equipeDTO.getId()).orElseThrow();

        } else {
            equipe = new Equipe();
        }
        equipe.setNom(equipeDTO.getNom());
        equipe.setEtudiantList(equipeDTO.getEtudiantList());

        return equipe;
    }
    public EquipeDTO toEquipeDTO(Equipe equipe){
        EquipeDTO equipeDTO=new EquipeDTO();

        equipeDTO.setId(equipe.getId());
        equipeDTO.setNom(equipe.getNom());
        equipeDTO.setProjet(equipe.getProjet());
        equipeDTO.setEtudiantList(equipe.getEtudiantList());

        return equipeDTO;
    }



    public void deleteEquipeById(Long id){
        equipeDAO.deleteById(id);}


    public List<Equipe> findAll() {
        return equipeDAO.findAll();
    }


    @Transactional
    public boolean affecterEtudiantAuProjet(Etudiant etudiant, Projet projet) {
        try {
            // Vérifiez si le projet a une équipe qui n'est pas complète
            Optional<Equipe> equipeOptionnelle = projet.getEquipeList().stream()
                    .filter(equipe -> !equipe.isComplet())
                    .findFirst();

            Equipe equipe;
            if (equipeOptionnelle.isPresent()) {
                // Si oui, affectez l'étudiant à cette équipe
                equipe = equipeOptionnelle.get();
            } else {
                // Sinon, créez une nouvelle équipe et associez-la au projet
                equipe = new Equipe();
                equipe.setProjet(projet);
                projet.getEquipeList().add(equipe);
                // Persistez les changements sur le projet
                projetService.save(projet);
            }

            // Affectez l'étudiant à l'équipe et sauvegardez
            etudiant.setEquipe(equipe);
            equipe.getEtudiantList().add(etudiant);
            equipeDAO.save(equipe);

            // Si tout s'est bien passé
            return true;
        } catch (Exception e) {
            // En cas d'exception, loggez l'erreur et retournez false
            // Log avec votre mécanisme de logging préféré, par exemple SLF4J
            // logger.error("Erreur lors de l'affectation de l'étudiant au projet", e);
            return false;
        }
    }


    public Equipe trouverDetailsEquipe(Long idProjet) {
        return equipeDAO.findByProjetId(idProjet)
                .orElseThrow(() -> new RuntimeException("Détails de l'équipe introuvables pour le projet id :" +
                        " " + idProjet));
    }









}
