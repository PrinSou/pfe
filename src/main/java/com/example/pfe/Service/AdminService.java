package com.example.pfe.Service;

import com.example.pfe.DAO.AdminDAO;


import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Admin;


import com.example.pfe.Model.Utilisateur;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdminService {

    private AdminDAO adminDAO;

    private PasswordEncoder encoder;

    @Autowired
    public AdminService(AdminDAO adminDAO,
            PasswordEncoder encoder) {
        this.adminDAO = adminDAO;
        this.encoder = encoder;
    }

        public List<Admin> getAll(){ return adminDAO.findAll();}

        public Admin getAdmin(Long id){
            Admin admin=adminDAO.findById(id).orElseThrow();
            return admin;
        }

        public   Admin getAdminById(Long id){
            return adminDAO.findById(id).orElse(null);
        }

        public Page<Admin> adminPage(String key, Pageable page){

            return this.adminDAO.findByNomContainingIgnoreCase(key,page);
        }


    public Admin save(UtilisateurDTO utilisateurDTO) {
        Admin admin = this.toAdmin(new UtilisateurDTO());
        return adminDAO.save(admin);
    }





        public  Admin saveAdmin(Admin admin
        ){ return adminDAO.save(admin);
        }

        public Utilisateur inscrireClient(UtilisateurDTO dto){
            Admin user=this.toAdmin(dto);
            user.setEmail(user.getEmail());
          //  user.setMdp(encoder.encode(user.getMdp()));
            System.out.println(user.getMdp());
            return adminDAO.save(user);
        }

    public Admin toAdmin(@Valid UtilisateurDTO utilisateurDTO) {
        Admin admin = new Admin();

        admin.setId(utilisateurDTO.getId());
        admin.setNom(utilisateurDTO.getNom());
        admin.setPrenom(utilisateurDTO.getPrenom());
        admin.setEmail(utilisateurDTO.getEmail());
        admin.setMdp(utilisateurDTO.getMdp());
        admin.setRole("ADMIN"); // Set role to ADMIN

        return admin;
    }

    public UtilisateurDTO toAdminDTO(Admin admin) {
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();

        utilisateurDTO.setId(admin.getId());
        utilisateurDTO.setNom(admin.getNom());
        utilisateurDTO.setPrenom(admin.getPrenom());
        utilisateurDTO.setEmail(admin.getEmail());
        utilisateurDTO.setRole(admin.getRole()); // Inclure le champ role

        return utilisateurDTO;
    }





        public void deleteAdminById(Long id){adminDAO.deleteById(id);}


}
