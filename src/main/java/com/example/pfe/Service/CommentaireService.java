package com.example.pfe.Service;
import com.example.pfe.DAO.CommentaireDAO;
import com.example.pfe.DTO.CommentaireDTO;
import com.example.pfe.Model.Commentaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class CommentaireService {

    private CommentaireDAO commentaireDAO;


    @Autowired
    public CommentaireService(CommentaireDAO commentaireDAO) {
        this.commentaireDAO = commentaireDAO;
    }



    public List<Commentaire> getAll(){ return commentaireDAO.findAll();}

    public Commentaire getCommentaire(Long id){
        Commentaire commentaire= commentaireDAO.findById(id).orElseThrow();
        return commentaire;
    }

    public Commentaire getCommentaireById(Long id){
        return commentaireDAO.findById(id).orElse(null);
    }

    public Page<Commentaire>commentairePage(String key, Pageable page){

        return this.commentaireDAO.findByNoteContainingIgnoreCase(key,page);
    }


    public  Commentaire save(CommentaireDTO commentaireDTO){
    Commentaire commentaire=this.toCommentaire(commentaireDTO);


        return commentaireDAO.save(commentaire);
    }
    public  Commentaire saveCommentaire(Commentaire commentaire){ return commentaireDAO.save(commentaire);
    }



    public Commentaire toCommentaire(CommentaireDTO commentaireDTO){
        Commentaire commentaire;
        if (commentaireDTO.getId()!=null){
            commentaire = commentaireDAO.findById(commentaireDTO.getId()).orElseThrow();
        }
        else {
            commentaire = new Commentaire();
        }
        commentaire.setNote(commentaireDTO.getNote());
        commentaire.setEquipe(commentaireDTO.getEquipe());
        commentaire.setUtilisateur(commentaireDTO.getUtilisateur());
        return commentaire;
    }

    public  CommentaireDTO toCommentaireDTO(Commentaire commentaire){
        CommentaireDTO commentaireDTO = new CommentaireDTO();

        commentaireDTO.setId(commentaire.getId());
        commentaireDTO.setNote(commentaire.getNote());
        commentaireDTO.setUtilisateur(commentaire.getUtilisateur());
        commentaireDTO.setEquipe(commentaire.getEquipe());

        return commentaireDTO;
    }





    public void deleteCommentaireById(Long id){commentaireDAO.deleteById(id);}
}