package com.example.pfe.Service;

import com.example.pfe.DAO.UtilisateurDAO;
import com.example.pfe.Model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

@Service
public class SecurityUserDetailsService implements UserDetailsService {
    private UtilisateurDAO utilisateurDAO;
    private PasswordEncoder encoder;
    @Autowired
    public SecurityUserDetailsService(UtilisateurDAO utilisateurDAO, PasswordEncoder encoder) {
        this.utilisateurDAO = utilisateurDAO;
        this.encoder = encoder;
    }
    /***
     * Une méthode qui prend en param un nom d'utilisateur ou email qui provient du formulaire de login.
     * Et retourne un objet User (class provenant de Security)  avec un username,password (encoder), une liste de permisions
     * @param username peudo ou email du form de login
     * @return  un objet User (qui implement l'interface UserDetails)
     * @throws UsernameNotFoundException
     **/
  @Override
    @Transactional(readOnly = true)
  //je créer une exception au démarrage si cette utilisateur existe déjà avec la méthode loadUserByUsername()
    public  UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      Utilisateur utilisateur = utilisateurDAO.findByEmail(username);
      if (utilisateur == null) {
          throw new UsernameNotFoundException("Utilisateur non trouvé" + username);
      }

      // Vérifie que l'adresse e-mail est valide
      // if (!StringUtils.isEmail(utilisateur.getEmail())) {
      //  throw new UsernameNotFoundException("L'adresse e-mail fournie n'est pas valide");
      // }

      // Vérifie que le mot de passe est valide
      //  String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#<span class="math-inline">%^&\+\=\]\)\(?\=\\\\S\+</span>).{8,}$";
      //  Pattern regex = Pattern.compile(pattern);
      // if (!regex.matcher(utilisateur.getMdp())){
      //  throw new UsernameNotFoundException("mot de passe fournie n'est pas valide");
      //  }
      //Récupère le rôle de l'utilisateur
      String nomRole = utilisateur.getClass().getSimpleName();
      //Etape 4 je consitu un lise de permision
      Set<GrantedAuthority> roles = new HashSet<>();
      //Etape 5 J'ajoute le nom du role dans la liste de permision.

      //si nomRole="Candidat" alors nomRole="ROLE_CANDIDAT" idem pour chacun des noms de roles
      //a partir du nom d'une classe, exemple "Candidat" construire une nouvelle chaine "ROLE_"+nomRole.toUppercase()
     // roles.add(SecurityUserDetailsService.getSimpleGrantedAuthority(nomRole));
      roles.add(new SimpleGrantedAuthority(nomRole.toUpperCase()));
      nomRole=nomRole.toUpperCase();
     // permisions.add(SecurityUserDetailsService.getSimpleGrantedAuthority(utilisateur.getClass().getSimpleName()));
     // permisions.add(new SimpleGrantedAuthority(ROLE_ADMIN));
      //roles.add(SecurityUserDetailsService.getSimpleGrantedAuthority(ROLE_CANDIDAT));
       // roles.add(SecurityUserDetailsService.getSimpleGrantedAuthority(ROLE_TUTEUR));
     // roles.add(SecurityUserDetailsService.getSimpleGrantedAuthority(ROLE_VISITEUR));
    //  //permisions.add(new SimpleGrantedAuthority(ROLE_CANDIDAT));
        /*permisions.add(new SimpleGrantedAuthority(ROLE_TUTEUR));
        permisions.add(new SimpleGrantedAuthority(ROLE_VISITEUR));*/
      //Etape 6 je créer un objet User
      return User.withUsername(utilisateur.getEmail())
              .password(utilisateur.getMdp())
              .roles(nomRole).build();
  }
    /**
     * Méthode pour construire un objet GrantedAuthority
     *
     * @param nomRole nom du rôle
     * @return objet GrantedAuthority
     */
   /* public static GrantedAuthority getSimpleGrantedAuthority(String nomRole) {
        return new SimpleGrantedAuthority("ROLE_" + nomRole.toUpperCase());
    }*/

    //GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
   // private static GrantedAuthority getSimpleGrantedAuthority(String ROLE_CANDIDAT) {
   // }
}

//spring extra tymeleaf security indepondance pour afficher le nom d'utulisateur
  /** je vais utuliser un th:text=avec une expression(#) ${#authentification ou authaurisation }donc je demande à soring securitu de
   m'afficher le nom de l'utulisateur qu est authentifié
   th:text="${#authentication.name}"
**/

  /**
   *   les 3 interface
   *   AuthenticationManager
   * AuthenticationFilter délégue la demande d'authentification à authentificationManager que lui même utilise
   * un provider qui traite la demande de l'authentification(exemple de provider: token, remember me; password...)
   * et UserDetailsService faut les configurer pour la sécurité
   *
   * l'interface userDetailService est tjrs associé à passwordencoder et cette interface à pour seul but c est de trouver l'utilisateur par son
   userName ou mail... et son password encoder**/




