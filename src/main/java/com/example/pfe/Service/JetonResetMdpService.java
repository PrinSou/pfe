package com.example.pfe.Service;

import com.example.pfe.DAO.JetonResetMdpDAO;
import com.example.pfe.Model.JetonResetMdp;
import com.example.pfe.Model.Utilisateur;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;
@Service
public class JetonResetMdpService {

@Autowired
    private JetonResetMdpDAO jetonResetMdpDAO;


        public JetonResetMdp createToken(Utilisateur utilisateur) {
        JetonResetMdp passwordResetToken = new JetonResetMdp();
        passwordResetToken.setUtilisateur(utilisateur);
        passwordResetToken.setToken(this.generateToken());

        // Définir la date d'expiration (par exemple, 1/2 heure à partir de maintenant)
        LocalDateTime expiryDate = LocalDateTime.now().plusMinutes(30);
        passwordResetToken.setExpiryDate(expiryDate);

        return jetonResetMdpDAO.save(passwordResetToken);
    }
    public JetonResetMdp findByToken(String jeton) {
        return jetonResetMdpDAO.findByToken(jeton);
    }

    public void deleteToken(JetonResetMdp jeton) {jetonResetMdpDAO.delete(jeton);
    }

    private String generateToken() {

        return UUID.randomUUID().toString();
    }

}
