package com.example.pfe.Service;


import com.example.pfe.DAO.UtilisateurDAO;
import com.example.pfe.DTO.PasswordDTO;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Admin;
import com.example.pfe.Model.Etudiant;
import com.example.pfe.Model.Professeur;
import com.example.pfe.Model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class UtilisateurService {

 private UtilisateurDAO utilisateurDAO;
 private PasswordEncoder encoder;

 @Autowired
 public UtilisateurService(UtilisateurDAO utilisateurDAO ,PasswordEncoder encoder) {
  this.utilisateurDAO = utilisateurDAO;
  this.encoder = encoder;
 }

 public Utilisateur inscrireClient(UtilisateurDTO utilisateurDTO) {
  Utilisateur user = this.convertToEntity(utilisateurDTO);
  user.setEmail(user.getEmail());
  //user.setMdp(encoder.encode(user.getMdp()));
  System.out.println(user.getMdp());
  return utilisateurDAO.save(user);
 }

 public Utilisateur convertToEntity(UtilisateurDTO dto) {
  Utilisateur utilisateur;
  if (dto.getId() == null) {
   if (dto.getRole().toUpperCase().equals("ADMIN")){
    utilisateur=new Admin(null,"So","BI","SO.BI@gmail.fr","12345678","12345678");
   } else if (dto.getRole()
           .toUpperCase().equals("TUTEUR")) {
    utilisateur =new Professeur(null, "Na", "bi","Na.bi@gmail.fr","12345678","12345678");

   }
   else {
    utilisateur =new Etudiant(null, "ti", "ines", "ti.ines@gmail.com", "123","123");
   }
  } else {
   utilisateur = utilisateurDAO.findById(dto.getId()).orElseThrow();
  }
  utilisateur.setEmail(dto.getEmail());
  //utilisateur.setMdp(encoder.encode(dto.getMdp1r()));
  return utilisateur;
 }

 public void supprimerUtilisateur(Long id) {
  utilisateurDAO.deleteById(id);
 }

 public Utilisateur modifierUtilisateur(UtilisateurDTO dto) {
  Utilisateur utilisateur = this.convertToEntity(dto);
  return utilisateurDAO.save(utilisateur);
 }


 //L'erreur dans la méthode trouverUtilisateurParNom() est que la variable utilisateur est déclarée mais non utilisée. La méthode devrait être modifiée comme suit :



 public Utilisateur trouverUtilisateurParNom(String Nom) {
  return (Utilisateur)  utilisateurDAO.findByNom(Nom);
 }
 public Utilisateur trouverUtilisateurParEmail(String Email) {
  return utilisateurDAO.findByEmail(Email);
 }

 public Utilisateur trouverUtilisateurParId(Long id) {
  return utilisateurDAO.findById(id).orElse(null);
 }


 public Utilisateur convertToEntity(PasswordDTO dto){
  Utilisateur utilisateur;
  if(dto.getToken()==null){
   throw new NoSuchElementException();
  }
  else{
   utilisateur= utilisateurDAO.findByJetonResetMdps_TokenIgnoreCase(dto.getToken()).orElseThrow();
   utilisateur.setMdp(dto.getMdp1());
  }

  return utilisateur;
 }

 public Utilisateur findByEmail(String email){
  return this.utilisateurDAO.findByEmail(email);
 }

 public Utilisateur updatePassword(Utilisateur user){
  if (user.getId() != null) {
   user.setMdp(encoder.encode(user.getMdp()));
   return utilisateurDAO.save(user);
  }

  throw new NoSuchElementException();
 }


}