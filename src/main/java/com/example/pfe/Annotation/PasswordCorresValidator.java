package com.example.pfe.Annotation;
import com.example.pfe.DTO.UtilisateurDTO;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordCorresValidator implements ConstraintValidator<PasswordCorres, UtilisateurDTO> {

    @Override
    public void initialize(PasswordCorres constraintAnnotation) {
    }

    @Override
    public boolean isValid(UtilisateurDTO utilisateurDTO, ConstraintValidatorContext context) {
        String mdp = utilisateurDTO.getMdp();
        String confirmMdp = utilisateurDTO.getConfirmMdp();

        return mdp != null && mdp.equals(confirmMdp);
    }
}
