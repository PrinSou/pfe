
package com.example.pfe.DTO;


import com.example.pfe.Model.Equipe;

import java.util.List;

public class FormUtilisateurAdminDTO extends UtilisateurDTO{

    public FormUtilisateurAdminDTO(Long id, String nom, String prenom, String email, String mdp, String confirmMdp, String role) {
        super(id, nom, prenom, email, mdp, confirmMdp, role);
        this.role = role;
    }

    public FormUtilisateurAdminDTO() {

    }

    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
