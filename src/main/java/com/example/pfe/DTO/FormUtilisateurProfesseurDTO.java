package com.example.pfe.DTO;

import com.example.pfe.Model.Equipe;

import java.util.List;

public class FormUtilisateurProfesseurDTO extends UtilisateurDTO {
    public FormUtilisateurProfesseurDTO(Long id, String nom, String prenom, String email, String mdp, String confirmMdp, String role) {
        super(id, nom, prenom, email, mdp, confirmMdp, role);
        this.role = role;
    }

    public FormUtilisateurProfesseurDTO() {
    }



    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}