package com.example.pfe.DTO;

import com.example.pfe.Annotation.PasswordCorres;
import com.example.pfe.Model.Equipe;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import javax.management.relation.Role;
import java.util.List;

//@PasswordCorres(message = "les mots de passe ne sont pas identiques")
public class UtilisateurDTO {

    private Long id;

    @NotBlank(message = "il faut un nom")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;


    @NotBlank(message = "il faut un prenom")
    @Size(min = 3,message = "le prenom est trop court")
    private String prenom;
    @Email(message = "il faut un email valide")
    private String email;

    @NotBlank(message = "il faut un mdp")
    @Size(min = 8)
    private String mdp;

    @NotBlank(message = "il faut comfirmer le mdp")
    @Size(min = 8)
    private String confirmMdp;

    public UtilisateurDTO() {
    }

    public UtilisateurDTO(Long id, String nom, String prenom, String email, String mdp, String confirmMdp, String role) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.mdp = mdp;
        this.confirmMdp = confirmMdp;

        this.role = role;
    }

    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getConfirmMdp() {
        return confirmMdp;
    }

    public void setConfirmMdp(String confirmMdp) {
        this.confirmMdp = confirmMdp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMdp() {
        return mdp;
    }







}
