package com.example.pfe.DTO;

import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Rentree;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.sql.Date;
import java.util.List;


public class ProjetDTO {
    private Long id;

    @NotBlank(message = "il faut un nom")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;


    private Date dateDebut;
    private Date dateFin;

    private Rentree rentree;

    public Rentree getRentree() {
        return rentree;
    }

    public void setRentree(Rentree rentree) {
        this.rentree = rentree;
    }

    public ProjetDTO(Long id, String nom, Date dateDebut, Date dateFin) {
            this.id = id;
            this.nom = nom;
            this.dateDebut = dateDebut;
            this.dateFin = dateFin;
        }


    public ProjetDTO() {
    }



     List<Equipe> equipeList;

    public List<Equipe> getEquipeList() {
        return equipeList;
    }

    public void setEquipeList(List<Equipe> equipeList) {
        this.equipeList = equipeList;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }


}


