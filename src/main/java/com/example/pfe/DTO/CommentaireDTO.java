
package com.example.pfe.DTO;

import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Utilisateur;

public class CommentaireDTO {

    private Long id;
    private String note;

    public CommentaireDTO() {
    }

    public CommentaireDTO(Long id, String note) {
        this.id = id;
        this.note = note;
    }

    private Equipe equipe;
    private Utilisateur utilisateur;




    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}