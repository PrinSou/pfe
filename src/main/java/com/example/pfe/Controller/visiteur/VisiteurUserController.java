package com.example.pfe.Controller.visiteur;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Etudiant;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Service.EtudiantService;
import com.example.pfe.Service.EquipeService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@AllArgsConstructor

public class VisiteurUserController {

    private EtudiantService etudiantService;



    /* @GetMapping("/inscription")
     public String inscription(@AuthenticationPrincipal User user) {
         if (!user.getAuthorities().containsAll(Arrays.asList(new SimpleGrantedAuthority("ROLE_VISITEUR"), new SimpleGrantedAuthority("ROLE_VISITEUR")))) {
             return "/visiteur/inscription";
         }

         return "inscription";
     }
 */
    @GetMapping("/login")
    public  String login(){
        return "visiteur/login";
    }

/*
    @GetMapping("/inscription")
    public String inscription(@ModelAttribute UtilisateurDTO utilisateurDTO, Model model) {
        List<Equipe> equipeList = equipeService.getAllEquipes();
        model.addAttribute("equipeList", equipeList);
        return "visiteur/inscription";
    }*/

   /* @PostMapping("/inscription")
    public String traitementInscription(Model model, @ModelAttribute @Valid UtilisateurDTO utilisateurDTO, BindingResult bindingResult, String nom, RedirectAttributes redirectAttributes) {
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("errorMessage", "Le formulaire comporte une erreur");
            model.addAttribute("utilisateurDTO",utilisateurDTO);
            return "visiteur/inscription";
        }

        // Vérifier si l'utilisateur a sélectionné une équipe
       // Cette condition vérifie si l'utilisateur a sélectionné une équipe. Si ce n'est pas le cas,
        // un message d'erreur est ajouté au résultat de la validation et l'utilisateur est redirigé
        // vers le formulaire d'inscription.
        if (nom == null || nom.isEmpty()) {
            bindingResult.addError(new FieldError("utilisateurDTO", "nom", "Veuillez sélectionner une équipe"));
            redirectAttributes.addFlashAttribute("errorMessage", "Le formulaire comporte une erreur");
            model.addAttribute("utilisateurDTO",utilisateurDTO);
            return "visiteur/inscription";
        }
        // Vérifier si l'équipe est complète
        //Cette ligne récupère l'équipe à partir de la base de données en fonction de son nom.
       // Equipe equipe= (Equipe) equipeService.getEquipeByNom(nom);
        //Cette condition vérifie si l'équipe est complète. Si c'est le cas, un message d'erreur est ajouté au résultat de la validation
        // et l'utilisateur est redirigé vers le formulaire d'inscription.
        for (Equipe uneEquipe: utilisateurDTO.getEquipeList()
             ) {

                if (uneEquipe.isComplet()) {
                    bindingResult.addError(new FieldError("utilisateurDTO", "nom", "L'équipe est complète"));
                    redirectAttributes.addFlashAttribute("errorMessage", "Le formulaire comporte une erreur");
                    model.addAttribute("utilisateurDTO", utilisateurDTO);
                    return "visiteur/inscription";
                }
            }

        Etudiant etudiant = etudiantService.inscrireEtudiant(utilisateurDTO);


        return "redirect:/home";
    }
*/






    }





