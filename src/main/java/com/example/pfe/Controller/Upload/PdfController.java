/*package com.example.pfe.Controller.Upload;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class PdfController {

    private final FilesImportService filesImportService;

    public PdfController(FilesImportService filesImportService) {
        this.filesImportService = filesImportService;
    }

    @PostMapping("/import-pdf")
    public void importPdf(@RequestParam("file") MultipartFile file) throws Exception {
        // Importez le fichier PDF
        filesImportService.importData(file.getInputStream());
    }

}*/