/**package com.example.pfe.Controller.Tuteur;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

@Controller

@RequestMapping("/tuteur")

public class TuteurUserController {

    @GetMapping("/tuteur")
    public String tuteur(@AuthenticationPrincipal User user) {
        if (!user.getAuthorities().containsAll(Arrays.asList(new SimpleGrantedAuthority("ROLE_TUTEUR"), new SimpleGrantedAuthority("ROLE_ADMIN")))) {
            return "/userInter/equipe";
        }

        return "tuteur";
    }
    @GetMapping
    public String index() {
        return "userInter/acceuil";
    }





}
*/