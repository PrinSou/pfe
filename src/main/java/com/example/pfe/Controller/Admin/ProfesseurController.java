
package com.example.pfe.Controller.Admin;


import com.example.pfe.DTO.UtilisateurDTO;

import com.example.pfe.Model.Professeur;
import com.example.pfe.Service.ProfesseurService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ProfesseurController {
    private ProfesseurService professeurService;

    @Autowired
    public ProfesseurController(ProfesseurService professeurService) {
        this.professeurService = professeurService;
    }

    @GetMapping("/admin/tuteur/tuteurs")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Professeur> professeurPage = professeurService.professeurPage(keyword, PageRequest.of(page, size));
        model.addAttribute("tuteurs", professeurPage.getContent());
        model.addAttribute("pages", new int[professeurPage.getTotalPages()]);
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);

        return "/admin/tuteur/tuteurs";
    }





    @GetMapping("/admin/tuteur/createTuteur")
    public String createTuteur(@ModelAttribute UtilisateurDTO utilisateurDTO) {

        return "admin/tuteur/createTuteur";
    }

    @PostMapping("/admin/tuteur/createTuteur")
    public String storeTuteur(Model model, @ModelAttribute @Valid UtilisateurDTO utilisateurDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
            model.addAttribute("tuteurs", professeurService.getAll());
            return "admin/tuteur/createTuteur";
        }
        Professeur professeur = professeurService.toProfesseur((UtilisateurDTO) utilisateurDTO);
        professeurService.save((UtilisateurDTO) utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Ajout d'un tuteur " + professeur.getNom());
        return "redirect:/admin/tuteur/tuteurs";

    }

    @GetMapping("/admin/tuteur/edit/{id}")
    public String editTuteur(@PathVariable long id, Model model) {
        Professeur professeur = professeurService.getProfesseur(id);
        UtilisateurDTO utilisateurDTO = professeurService.toProfesseurDTO(professeur);
        model.addAttribute("tuteur", professeurService.getAll());
        model.addAttribute("utilisateurDTO", utilisateurDTO);


        return "/admin/tuteur/editTuteur";
    }


    @PostMapping("/admin/tuteur/tuteurs")
    public String uppTuteur(Model model, @ModelAttribute UtilisateurDTO utilisateurDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("tuteur", professeurService.getAll());
            return "/admin/tuteur/editTuteur";
        }
        professeurService.save((UtilisateurDTO) utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Modification d'un tuteur " + utilisateurDTO.getNom());
        return "redirect:/admin/tuteur/tuteurs";
    }
    @GetMapping("/admin/tuteur/showTuteur")
    public String showTuteur(@PathVariable Long id) {
        Professeur professeur = professeurService.getProfesseurById(id);
        return "/admin/tuteur/showTuteur";
    }

    @PostMapping("admin/tuteur/tuteurs/delete")
    public String delete(@RequestParam Long id, RedirectAttributes redirectAttributes) {
        /** @RequestParam(name="keyword" defaultValue="" ) String keyword,
         @RequestParam(name="page" defaultValue="0" )
         int page);**/

        this.professeurService.deleteProfesseurById(id);
        redirectAttributes.addFlashAttribute("successMessage", "Suppresion d'un tuteur");
        return "redirect:/admin/tuteur/tuteurs";
        /**    ?page="+page+"keyword"+keyword;**/

    }

}
