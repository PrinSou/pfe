package com.example.pfe.Controller.Admin;

import com.example.pfe.DTO.ProjetDTO;
import com.example.pfe.Model.Projet;

import com.example.pfe.Service.EtudiantService;
import com.example.pfe.Service.ProjetService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class ProjetController {

    private ProjetService projetService;

    @Autowired
/***
 * @Authowired dire à spring injecte moi le DAO
 */
    public ProjetController(ProjetService projetService) {
        this.projetService = projetService;
    }

    /**
     * @Autowired pour l'injection des dépondance
     * mais spring préfere faire l'injection via un constructeur avec paramétre==>l'injection fait via le constructeur
     */


    /**
     * créer une méthode qui retourne un String qui s'appel index()
     * et qui nous retourneras une vue appele "projets"
     * ==>la liste: this.projetService.getAllProjets() je la stocke dans le model
     * donc faut déclarer un objet de type model
     * on stocke le resultat "projets" dans le model
     * <p>
     * int page, int size pour la pagination, la variable page coreespond au numero de la page et size correspond
     * au nbr de donnée affichées par page
     *
     * @RequestParam c est une annotation pour dire que si la variable ne porte pas le même nom que le paramétre qu 'est dnaq
     * qu'est dans l'url donc on utulisons cette annotation tu vas lui dire va récupérer le paramétre "page"
     * qu'est dans ton url et tu l'affecte à la variable int page et si tu trouve pas ce paramétre la valeur par défaut est "0"
     * la méthode getTotalPages() va retourner le nbr total de pages
     * on demande au modéle de leur passer au template
     * Récupére la liste des projets avec la méthode getAll() par le biasi du projetService et les stocké dans
     * la variable projetsList une fois c est fais je vais les passé au model que lui même
     * les ajouteras au template
     */


    @GetMapping("/admin/projet/projets")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Projet> pageProjets = projetService.pageProjets(keyword, PageRequest.of(page, size));
        model.addAttribute("projets", pageProjets.getContent());

        /**
         * je veux parcourir toute les projets indiquer par la variable "projets" et comme 2 paramétres
         * indiquer la listeProjets qui permet les parcourir
         */


        /**
         * la pagination j'ai besoin de stocker dans le model le nbr de pages donc je crée un tableau
         * créer un attribut qui s'appel "pages" dans la dimenssion c est int et c est le nbre de pages
         */
        model.addAttribute("pages", new int[pageProjets.getTotalPages()]);

        /**
         * j'ai besoin de stocker dans le model la page courrante
         *donc à chaque fois je garde dans le model la page actuelle colore(la page que j'ai demandé)
         */

        /**
         * spring data JPA utulise DAO et le DAO utulise l'orm
         *
         */
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);

        return "/admin/projet/projets";
    }


    @GetMapping("/international")
    public String getInternationalPage() {
        return "international";
    }

    /**
     * @ModelAttribute permet à spring de récupérer les données saisies dans les champs du  formulaire
     * et de construire un objet employé avec.
     *
     * afficher
     **/
    /**
     *   model.addAttribute("projet",projet);
     * le 2 paramétre correspond à ma variable java
     * "projet" la chaine de caractére ce que j vais utulisé dans le template
     * @param model
     * @return
     */

  /**  @GetMapping("/admin/projet/projets/{id}")

    public String showProjet(@PathVariable Long id,  Model model) {
        Projet projet = projetService.findById(id);
        ProjetDTO projetDTO=projetService.toDTO(projet);
        model.addAttribute("projet",projetDTO);
        return "admin/projet/showProjet";
    }
**/


  @GetMapping("/admin/projet/createProjet")

    public String createProjet(@ModelAttribute ProjetDTO projetDTO) {
        LocalDate current_date = LocalDate.now();
        int annee = current_date.getYear();
        projetDTO.setNom("projet Tennis"+annee);
        return "admin/projet/createProjet";
    }


    /**
     * la méthode storeProjet le type de retour est String ce qui signifie elle renvoie
     * une chaine de caractére
     * les parenthéses `(...)` indiquent que la méthode peut prendre des paramètres
     *stocker
     *
**/

    @PostMapping ("/admin/projet/createProjet")
    public String storeProjet(@ModelAttribute @Valid ProjetDTO projetDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "admin/projet/createProjet";
        }
        projetService.save(projetDTO);
        return "redirect:/admin/projet/projets";

    }



    /**
     *
     * La méthode save est annotée @PostMapping, il s’agit ici de traiter la validation
     * *d’un formulaire, et généralement les formulaires exécutent des requêtes POST.
     * @param
     * @param
     * @return**/
    @GetMapping("/admin/projet/showProjet")
       public String showProjet(@PathVariable Long id) {
           Projet projet = projetService.getProjet(id);
           return "admin/projet/show_Projet";
       }



      /** @PostMapping("/projets") pas maintenat
    public String saveProjet(@ModelAttribute("projet") ProjetDTO projetDTO){
           projetService.saveProjet(projet);
           return "redirect:/projets";
       }
**/

      /**l'URL :/projets/edit/{id}**/

   @GetMapping("/admin/projets/edit/{id}")
    public String editProjet(@PathVariable long id,Model model) {
       Projet projet =projetService.getProjet(id);
       ProjetDTO projetDTO=projetService.toDTO(projet);
       model.addAttribute("projet",projetService.getAll());
       model.addAttribute("projetDTO",projetDTO);


           return "/admin/projet/editProjet";
       }


    @PostMapping("/admin/projet/projets")
    public String uppProjet( Model model,@ModelAttribute  ProjetDTO projetDTO,BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("projets", projetService.getAll());
            return "/admin/projet/editProjet";
        }
       /** Projet projet = projetService.toProjet(projetDTO);**/
        projetService.save(projetDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Modification de la session " + projetDTO.getNom());
        return "redirect:/admin/projet/projets";
    }



/**
     j'envoi la requette /delete c est la méthode deleteProjetById qui va être s'éxcuter
    on récupérant l'id à supprimer
     et nous renvoie vers la pge "projets" ou il y a tout le reste des projets
     ==>?page="+page+"keyword"+keyword; pour rester sur la même page après supprissions

**/


    @PostMapping("admin/projet/projets/delete")
    public String delete(@RequestParam Long id,RedirectAttributes redirectAttributes ){
   /** @RequestParam(name="keyword" defaultValue="" ) String keyword,
     @RequestParam(name="page" defaultValue="0" )
    int page);**/

        this.projetService.deleteProjetById(id);
        redirectAttributes.addFlashAttribute("successMessage","Suppresion du projet");
        return "redirect:/admin/projet/projets";
    /**    ?page="+page+"keyword"+keyword;**/

    }


}