package com.example.pfe.Controller.Admin;


import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Admin;
import com.example.pfe.Service.AdminService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AdminController {

        AdminService adminService;
    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }
    @GetMapping("/admin/dashboard")
    public String adminDash(){
        return "admin/dashboard";
    }

    @GetMapping("/admin/index")
        public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                            @RequestParam(name = "size", defaultValue = "4") int size,
                            @RequestParam(name = "keyword", defaultValue = "") String keyword) {
            Page<Admin> adminPage = adminService.adminPage(keyword, PageRequest.of(page, size));
            model.addAttribute("admins", adminPage.getContent());
            model.addAttribute("pages", new int[adminPage.getTotalPages()]);
            model.addAttribute("currentPage", page);
            model.addAttribute("keyword", keyword);

            return "dashboard";
        }




        @GetMapping("/admin/adminis/createAdmin")
        public String createAdmin(@ModelAttribute UtilisateurDTO utilisateurDTO) {

            return "admin/adminis/createAdmin";
        }

        @PostMapping("/admin/adminis/createAdmin")
        public String storeAdmin(Model model, @ModelAttribute @Valid UtilisateurDTO utilisateurDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
            if (bindingResult.hasErrors()) {
                model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
                model.addAttribute("admins", adminService.getAll());
                return "admin/adminis/createAdmin";
            }

            Admin admin = adminService.toAdmin(utilisateurDTO);
            adminService.save(utilisateurDTO);
            redirectAttributes.addFlashAttribute("successMessage", "Ajout d'un candidat " + admin.getNom());
            return "redirect:/admin/adminis/administras";

        }

        @GetMapping("/admin/adminis/edit/{id}")
        public String editAdmin(@PathVariable long id, Model model) {
            Admin admin=adminService.getAdmin(id);
            UtilisateurDTO utilisateurDTO=adminService.toAdminDTO(admin);
            model.addAttribute("admin",adminService.getAll());
            model.addAttribute("adminDTO",utilisateurDTO);


            return "/admin/adminis/editAdmin";
        }


        @PostMapping("/admin/adminis/cadministras")
        public String uppAdmin( Model model,@ModelAttribute  UtilisateurDTO adminDTO ,BindingResult bindingResult, RedirectAttributes redirectAttributes) {
            if (bindingResult.hasErrors()) {
                model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
                model.addAttribute("admin", adminService.getAll());
                return "/admin/adminis/editAdmin";
            }
            adminService.save(adminDTO);
            redirectAttributes.addFlashAttribute("successMessage", "Modification d'un admin " + adminDTO.getNom());
            return "redirect:/admin/adminis/administras";
        }

        @PostMapping("admin/adminis/administras/delete")
        public String delete(@RequestParam Long id,RedirectAttributes redirectAttributes ){
            /** @RequestParam(name="keyword" defaultValue="" ) String keyword,
             @RequestParam(name="page" defaultValue="0" )
             int page);**/

            this.adminService.deleteAdminById(id);
            redirectAttributes.addFlashAttribute("successMessage","Suppresion d'un admin");
            return "redirect:/admin/adminis/administras";
            /**    ?page="+page+"keyword"+keyword;**/

        }
    }

