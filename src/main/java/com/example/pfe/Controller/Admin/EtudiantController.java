package com.example.pfe.Controller.Admin;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Etudiant;
import com.example.pfe.Service.EtudiantService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
@Controller
public class EtudiantController {
    EtudiantService etudiantService;
    @Autowired
    public EtudiantController(EtudiantService etudiantService) {
        this.etudiantService = etudiantService;
    }
    @GetMapping("/admin/etudiant/etudiants")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Etudiant> etudiantsPage = etudiantService.etudiantsPage(keyword, PageRequest.of(page, size));
        model.addAttribute("etudiants", etudiantsPage.getContent());
        model.addAttribute("pages", new int[etudiantsPage.getTotalPages()]);
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);

        return "/admin/etudiant/etudiants";
    }
    /**
     * Affiche le formulaire d'inscription
     *
     */
    @GetMapping("/admin/etudiant/createEtudiant")
    public String createEtudiant(@ModelAttribute UtilisateurDTO utilisateurDTO) {
        return "admin/etudiant/createEtudiant";
    }
    @PostMapping("/admin/etudiant/createEtudiant")
    public String storeEtudiant(Model model, @ModelAttribute @Valid UtilisateurDTO utilisateurDTO, BindingResult bindingResult,
                                RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
            model.addAttribute("etudiants", etudiantService.getAll());
            return "admin/etudiant/createEtudiant";
        }
        Etudiant etudiant = etudiantService.toEtudiant(utilisateurDTO);
        etudiantService.inscrireEtudiant(utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Ajout d'un candidat " + etudiant.getNom());
        return "redirect:/admin/etudiant/etudiants";}
    @GetMapping("/admin/etudiant/edit/{id}")
    public String editCandidat(@PathVariable long id, Model model) {
        Etudiant etudiant = etudiantService.getEtudiant(id);
        UtilisateurDTO utilisateurDTO= etudiantService.toEtudiantDTO(etudiant);
        model.addAttribute("etudiants", etudiantService.getAll());
        model.addAttribute("UtilisateurDTO");
        return "/admin/etudiant/editEtudiant";}
    @PostMapping("/admin/etudiant/etudiants")
    public String uppEtudiant( Model model,@ModelAttribute  UtilisateurDTO utilisateurDTO ,BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("etudiant", etudiantService.getAll());
            return "/admin/etudiant/editEtudiant";
        }
        etudiantService.inscrireEtudiant( utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Modification d'un candidat " + utilisateurDTO.getNom());
        return "redirect:/admin/etudiant/etudiants";
    }
        @PostMapping("admin/etudiant/etudiants/delete")
        public String delete(@RequestParam Long id,
                @RequestParam(name="keyword", defaultValue="") String keyword,
        @RequestParam(name="page", defaultValue="0") int page,
        RedirectAttributes redirectAttributes) {
            this.etudiantService.deleteEtudiantById(id);
            redirectAttributes.addFlashAttribute("successMessage", "Suppression de l'etudiant");
            return "redirect:/admin/etudiant/etudiants?page=" + page + "&keyword=" + keyword;
        }

    }



