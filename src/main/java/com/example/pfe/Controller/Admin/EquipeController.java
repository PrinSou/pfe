package com.example.pfe.Controller.Admin;


import com.example.pfe.DAO.EquipeDAO;
import com.example.pfe.DTO.EquipeDTO;
import com.example.pfe.DTO.ProjetDTO;
import com.example.pfe.DTO.RentreeDTO;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Projet;
import com.example.pfe.Model.Rentree;
import com.example.pfe.Service.EquipeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class EquipeController {


    private EquipeService equipeService;

    @Autowired
    public EquipeController(EquipeService equipeService) {
        this.equipeService = equipeService;
    }

    @GetMapping("/admin/equipe/equipes")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Equipe> pageEquipes = equipeService.pageEquipes(keyword, PageRequest.of(page, size));
        model.addAttribute("equipe", pageEquipes.getContent());

        model.addAttribute("pages", new int[pageEquipes.getTotalPages()]);
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);
        return "admin/equipe/equipes";

    }

    @GetMapping("/admin/equipe/equipes/{id}")

    public String showEquipe(@PathVariable Long id, Model model) {
        Equipe equipe = equipeService.getEquipeById(id);
        EquipeDTO equipeDTO = equipeService.toEquipeDTO(equipe);
        model.addAttribute("equipe", equipeDTO);
        return "admin/equipe/showEquipe";
    }


    @GetMapping("/admin/equipe/createEquipe")
    String createEquipe(@ModelAttribute EquipeDTO equipeDTO) {
        equipeDTO.getNom();
        return "admin/equipe/createEquipe";
    }


    @PostMapping("/admin/equipe/createEquipe")
    public String store(Model model, @ModelAttribute @Valid EquipeDTO equipeDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
            model.addAttribute("equipes", equipeService.getAllEquipes());
            return "/admin/equipe/createEquipe";
        }
        Equipe equipe = equipeService.toEquipe(equipeDTO);
        equipeService.save(equipeDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Ajout de la rentree " + equipe.getNom());
        return "redirect:/admin/equipe/equipes";
    }


    @GetMapping("/admin/equipes/edit/{id}")
    public String editEquipe(@PathVariable long id, Model model) {
        Equipe equipe = equipeService.getEquipeById(id);
        EquipeDTO equipeDTO = equipeService.toEquipeDTO(equipe);
        model.addAttribute("equipe", equipeService.getAllEquipes());
        model.addAttribute("equipeDTO", equipeDTO);

        return "/admin/equipe/editEquipe";
    }

    @PostMapping("/admin/equipe/equipes")
    public String uppEquipe(Model model, @ModelAttribute EquipeDTO equipeDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("equipes", equipeService.getAllEquipes());
            return "/admin/equipe/editEquipe";
        }

        equipeService.save(equipeDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Modification d' équipe"
                + equipeDTO.getNom());
        return "redirect:/admin/equipe/equipes";

    }


    @PostMapping("/equipes/delete/{id}")
    public String deleteEquipeById(@PathVariable Long id) {
        equipeService.deleteEquipeById(id);
        return "redirect:/admin/equipe/equipes";

    }
}





