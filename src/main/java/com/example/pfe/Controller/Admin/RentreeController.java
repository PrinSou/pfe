
package com.example.pfe.Controller.Admin;

import com.example.pfe.DAO.RentreeDAO;
import com.example.pfe.DTO.RentreeDTO;
import com.example.pfe.Model.Rentree;
import com.example.pfe.Service.RentreeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RentreeController {

    private RentreeService rentreeService;

@Autowired
    public RentreeController(RentreeService rentreeService) {
        this.rentreeService = rentreeService;
    }

    /**
     * @GetMapping("/admin/sessionA/sessionsA") public String index(Model model) {
     * model.addAttribute("sessions", sessionService.getAllSessions());
     * return "admin/session/sessions";
     * }
     **/

@GetMapping("/admin/rentree/rentrees")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Rentree> rentreePage = rentreeService.     rentreePage(keyword, PageRequest.of(page, size));
        model.addAttribute("rentrees", rentreePage.getContent());

        /**
         * je veux parcourir toute les projets indiquer par la variable "projets" et comme 2 paramétres
         * indiquer la listeProjets qui permet les parcourir
         */


        /**
         * la pagination j'ai besoin de stocker dans le model le nbr de pages donc je crée un tableau
         * créer un attribut qui s'appel "pages" dans la dimenssion c est int et c est le nbre de pages
         */
        model.addAttribute("pages", new int[rentreePage.getTotalPages()]);

        /**
         * j'ai besoin de stocker dans le model la page courrante
         *donc à chaque fois je garde dans le model la page actuelle colore(la page que j'ai demandé)
         */

        /**
         * spring data JPA utulise DAO et le DAO utulise l'orm
         *
         */
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);

        return "admin/rentree/rentrees";
    }

    @GetMapping("/admin/rentree/createRE")

    public String createRE(Model model){
        model.addAttribute("rentreeDTO",new RentreeDTO());
        model.addAttribute("rentrees", rentreeService.getAll());
        return "/admin/rentree/createRE";
    }


    @PostMapping("/admin/rentrees")
    public String store (Model model, @ModelAttribute @Valid RentreeDTO rentreeDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
            model.addAttribute("rentrees", rentreeService.getAll());
            return "admin/rentree/createRE";
        }
        Rentree rentree = rentreeService.toRentree(rentreeDTO);
        rentreeService.save(rentreeDTO);
        redirectAttributes.addFlashAttribute("successMessage","Ajout de la session "+ rentree.getId());
        return "redirect:/admin/rentrees";
    }





/**
    @GetMapping("/admin/rentree/rentrees/{id}")

    public String showRe(@PathVariable Long id, /**Model model) {
        Rentree rentree = rentreeService.getRentreeById(id);
        RentreeDTO rentreeDTO = rentreeService.toRentreeDTO(rentree);

       model.addAttribute("se", rentreeDTO);

        return "admin/rentree/showRe";
    }
**/





    @GetMapping("/rentrees/edit/{id}")
    public String editRE(@PathVariable Long id, Model model) {
    Rentree rentree = rentreeService.getRentreeById(id);
    RentreeDTO rentreeDTO = rentreeService.toRentreeDTO(rentree);
        model.addAttribute("rentrees", rentreeService.getAll());
        model.addAttribute("rentreDTO" , rentreeDTO);
        return "editRE";
    }


    @PostMapping("/rentrees/{id}")

    public String updateRentree(@ModelAttribute @Valid RentreeDTO rentreeDTO, Model model, BindingResult bindingResult, RedirectAttributes redirectAttributes ) {

        // get projet from database by id
        if(bindingResult.hasErrors()){
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("rentree", rentreeService.getAll());
            return "/admin/rentree/editRE";
        }
        Rentree rentree = rentreeService.toRentree(rentreeDTO);
       rentreeService.save(rentreeDTO);
        redirectAttributes.addFlashAttribute("successMessage","Modification de la session "+ rentree.getId());
        return "redirect:/admin/rentrees";
    }


    @GetMapping("/rentrees/{id}")
    public String deleteRentree(@PathVariable Long id){
        rentreeService.deleteRentreeById(id);
        return  "redirect:/rentrees";
    }

}
