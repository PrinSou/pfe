
package com.example.pfe.Controller.Admin;

import com.example.pfe.DTO.CommentaireDTO;
import com.example.pfe.Model.Commentaire;
import com.example.pfe.Service.CommentaireService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller

public class CommentaireController {
    private CommentaireService commentaireService;
    @Autowired


    public CommentaireController(CommentaireService commentaireService) {
        this.commentaireService = commentaireService;
    }


    @GetMapping("/admin/commentaire/commentaires")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Commentaire>commentairePage = commentaireService.commentairePage(keyword, PageRequest.of(page, size));
        model.addAttribute("commentaires", commentairePage.getContent());

        model.addAttribute("pages", new int[commentairePage.getTotalPages()]);
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);
        return "admin/commentaire/commentaires";
    }
        @GetMapping( "/admin/commentaire/commentaires/{id}")
        public String showComm(@PathVariable Long id, Model model) {
            Commentaire commentaire = commentaireService.getCommentaireById(id);
            CommentaireDTO commentaireDTO = commentaireService.toCommentaireDTO(commentaire);
            model.addAttribute("commentaire", commentaireDTO);
            return "admin/commentaire/showComm";
        }


        @GetMapping("/admin/commentaire/createComm")
        String createComm(@ModelAttribute CommentaireDTO commentaireDTO) {

            return "admin/commentaire/createComm";
        }


        @PostMapping("/admin/commentaire/createComm")
        public String store(Model model, @ModelAttribute @Valid CommentaireDTO commentaireDTO, BindingResult
        bindingResult, RedirectAttributes redirectAttributes) {
            if (bindingResult.hasErrors()) {
                model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
                model.addAttribute("commentaires", commentaireService.getAll());
                return "/admin/commentaire/createComm";
            }
            Commentaire commentaire= commentaireService.toCommentaire(commentaireDTO);
            commentaireService.save(commentaireDTO);
            redirectAttributes.addFlashAttribute("successMessage", "Ajout d'un commentaire " + commentaire.getNote());
            return "redirect:/admin/commentaire/commentaires";
        }


        @GetMapping("/admin/commentaires/edit/{id}")
        public String editComm(@PathVariable long id, Model model) {
            Commentaire commentaire = commentaireService.getCommentaireById(id);
            CommentaireDTO commentaireDTO= commentaireService.toCommentaireDTO(commentaire);
            model.addAttribute("commentaire", commentaireService.getAll());
            model.addAttribute("commentaireDTO", commentaireDTO);

            return "/admin/commentaire/editComm";
        }

        @PostMapping("/admin/commentaire/commentaires")
        public String uppComm(Model model, @ModelAttribute CommentaireDTO commentaireDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
            if (bindingResult.hasErrors()) {
                model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
                model.addAttribute("commentaires", commentaireService.getAll());
                return "/admin/commentaire/editComm";
            }

            commentaireService.save(commentaireDTO);
            redirectAttributes.addFlashAttribute("successMessage", "Modification d'un commentaire"
                    + commentaireDTO.getNote());
            return "redirect:/admin/commentaire/commentaires";

        }


        @PostMapping("/commentaire/delete/{id}")
        public String deleteComm(@PathVariable Long id) {
            commentaireService.deleteCommentaireById(id);
            return "redirect:/admin/commentaire/commentaires";

        }
}
