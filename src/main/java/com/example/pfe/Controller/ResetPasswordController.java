package com.example.pfe.Controller;

import com.example.pfe.DAO.JetonResetMdpDAO;
import com.example.pfe.DTO.PasswordDTO;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.JetonResetMdp;
import com.example.pfe.Model.Utilisateur;
import com.example.pfe.Service.JavaSenderMail;
import com.example.pfe.Service.JetonResetMdpService;
import com.example.pfe.Service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;



@Controller
    public class ResetPasswordController {


        private JetonResetMdpService tokenService;

        private JavaSenderMail javaSenderMail;


        private UtilisateurService utilisateurService;

        private JetonResetMdpDAO jetonResetMdpDAO;
@Autowired
        public ResetPasswordController(JetonResetMdpService tokenService, JavaSenderMail javaSenderMail, UtilisateurService utilisateurService, JetonResetMdpDAO jetonResetMdpDAO) {
            this.tokenService = tokenService;
            this.javaSenderMail = javaSenderMail;
            this.utilisateurService = utilisateurService;
            this.jetonResetMdpDAO = jetonResetMdpDAO;
        }





      /*   * Formulaire en cas d'oublie
         * @return*/


        /*@GetMapping("/forgot-password")
        public String showForgotPasswordForm() {
            return "/visiteur/mdpOublier";
        }
*/

        /* * Traitement du formulaire
         * @param email
         * @return
*/
        @GetMapping("/visiteur/mdpOublier")
        public String showForgotPasswordForm(Model model) {
            model.addAttribute("passwordDTO", new PasswordDTO());
            return "reset-password";

        }


        @PostMapping("/forgot-password")
        public String processForgotPassword(@RequestParam("email") String email, @ModelAttribute PasswordDTO passwordDTO) throws Exception {
            Utilisateur utilisateur = utilisateurService.findByEmail(email);
            if (utilisateur != null) {
                // Générez un jeton de réinitialisation de mot de passe
                JetonResetMdp jetonResetMdp = tokenService.createToken(utilisateur);

                // Envoyez le jeton au courriel de l'utilisateur et incluez un lien pour réinitialiser le mot de passe
                javaSenderMail.sendPasswordResetEmail(utilisateur.getEmail(), jetonResetMdp.getToken());

                // Envoyez également le lien vers une page de réinitialisation de mot de passe
                return "redirect:/forgot-password?success";
            }

            return "redirect:/forgot-password?error";
        }

        @GetMapping("/reset-password")
        public String showResetPasswordForm(@RequestParam("token") String token, Model model) {
            JetonResetMdp resetToken = tokenService.findByToken(token);
            if (resetToken != null) {
                PasswordDTO dto= new PasswordDTO(token);
                model.addAttribute("passwordDto", dto);
                return "/visiteur/resetMotDePasse";
            }
            return "redirect:/forgot-password?invalidToken";
        }

    @PostMapping("/reset-password")
    public String processResetPassword(@ModelAttribute PasswordDTO passwordDTO)  {
        JetonResetMdp resetToken = tokenService.findByToken(passwordDTO.getToken());
        if (resetToken != null) {
            //TODO verification de l'expiration du token
            if (resetToken.getExpiryDate().isBefore(LocalDateTime.now())) {
                // Le jeton a expiré
                return "redirect:/login?passwordResetTokenExpired";
            }

            Utilisateur utilisateur=utilisateurService.convertToEntity(passwordDTO);
            utilisateurService.inscrireClient(new UtilisateurDTO());
            tokenService.deleteToken(resetToken);
            return "redirect:/login?passwordResetSuccess";
        }
        return "redirect:/login?passwordResetError";
    }



    }


