package com.example.pfe.Controller.Etudiant;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Etudiant;
import com.example.pfe.Model.Projet;
import com.example.pfe.Service.EtudiantService;
import com.example.pfe.Service.EquipeService;
import com.example.pfe.Service.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/etudiant")
public class EtudiantUserController {
   final private EtudiantService etudiantService;
   private EquipeService equipeService;
    public EtudiantUserController(EtudiantService etudiantService, EquipeService equipeService) {
        this.etudiantService = etudiantService;
        this.equipeService = equipeService;}
    @GetMapping("/acceuil")
        public String index() {
            return "/front/home";
        }
    @GetMapping("/equipe")
    public String teams(Model model) {
        // Vérifiez que la liste des équipes n'est pas nulle
        if (equipeService.getAllEquipes() == null) {
            return "/error";}
        // Utilisez une méthode de requête `findAll()` plus concise pour récupérer la liste des équipes
        List<Equipe> equipeList = equipeService.getAllEquipes();
        // Ajoutez la liste des équipes au modèle
        model.addAttribute("equipeList", equipeList);
        // Retournez la vue `/front/team`
        return "/front/team";}
    @GetMapping("/equipe/{id}")
    public String teamDetails(@PathVariable Long id, Model model) {
        Equipe equipe = equipeService.trouverDetailsEquipe(id);
        if (equipe != null) {
            model.addAttribute("equipe", equipe);
            return "/front/teamDetails";
        } else {
            model.addAttribute("erreur", "Équipe non trouvée.");
            return "/error";
        }
    }


    @Autowired
    private ProjetService projetService;
    @GetMapping("/choisirProjet")
    public String choisirProjet(Model model, Principal principal) {
        String nomUtilisateur = principal.getName();
        Etudiant etudiantConnecte = etudiantService.findByUsername(nomUtilisateur);
        if (etudiantConnecte != null) {
            List<Projet> projetsDisponibles = projetService.listeProjetsDisponibles();
            Map<Long, Boolean> selectionProjets = new HashMap<>();
            projetsDisponibles.forEach(projet -> selectionProjets.put(projet.getId(), false));
            model.addAttribute("projets", projetsDisponibles);
            model.addAttribute("selectionProjets", selectionProjets);
            model.addAttribute("idEtudiant", etudiantConnecte.getId());
        } else {
            model.addAttribute("erreur", "Étudiant non trouvé.");
            return "vueErreur";}
        return "front/choixProjetEtudiant";}
    @PostMapping("/selectionnerProjet")
    public String selectionnerProjet(@RequestParam("projetSelectionne") Long idProjet, Model model, Principal principal) {
        String nomUtilisateur = principal.getName();
        Etudiant etudiantConnecte = etudiantService.findByUsername(nomUtilisateur);
        if (etudiantConnecte != null) {
            Projet projetChoisi = projetService.findById(idProjet);
            if (projetChoisi != null) {
                boolean affectation = equipeService.affecterEtudiantAuProjet(etudiantConnecte, projetChoisi);
                if (affectation) {
                    return "redirect:/etudiant/equipeDetails?idProjet=" + projetChoisi.getId();
                    //return "front/equipeDetails";
                } else {
                    // L'affectation a échoué, restez sur la même vue avec un message d'erreur
                    model.addAttribute("erreur", "L'affectation a échoué.");
                    return "front/choixProjetEtudiant";}
            } else {
                model.addAttribute("erreur", "Projet non trouvé.");
                return "front/choixProjetEtudiant";}
        } else {
            model.addAttribute("erreur", "Étudiant non connecté.");
            return "front/choixProjetEtudiant";}}
    // Supposons que vous avez un @ModelAttribute pour les projets disponibles
    @ModelAttribute("projetsDisponibles")
    public List<Projet> projetsDisponibles() {
        return projetService.listeProjetsDisponibles();}
 /*   @GetMapping("/equipeDetails")
    public String equipeDetails(@RequestParam("idProjet") Long idProjet, Model model) {
        Equipe equipe = equipeService.trouverDetailsEquipe(idProjet);
        if (equipe != null) {
            model.addAttribute("equipe", equipe);
        } else {
            model.addAttribute("erreur", "Les détails de l'équipe ne sont pas disponibles pour ce projet.");}
        return "front/teamDetails";}*/
 @GetMapping("/equipeDetails")
 public String equipeDetails(@RequestParam("idProjet") Long idProjet, Model model) {
     Equipe equipe = equipeService.trouverDetailsEquipe(idProjet);
     if (equipe != null && equipe.getProjet() != null) {
         // Ajoutez des logs ici pour le débogage
         System.out.println("Équipe: " + equipe.getNom() + ", Projet: " + equipe.getProjet().getNom());
         model.addAttribute("equipe", equipe);
     } else {
         // Log l'erreur ou ajoutez plus d'informations sur l'erreur dans le modèle
         System.out.println("Erreur: Les détails de l'équipe ou du projet sont manquants pour l'idProjet " + idProjet);
         model.addAttribute("erreur", "Les détails de l'équipe ou du projet sont manquants.");
         return "error";
     }
     return "front/teamDetails";
 }



}


