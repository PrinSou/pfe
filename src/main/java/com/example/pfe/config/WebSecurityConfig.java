
package com.example.pfe.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    public PasswordEncoder getEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    //  est un objet htttpSecurity de type httpSecurity  qui va sécuriser les requettes http
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity

                //c est pour créer un filtre on utulise la méthode SecurityFilterChain
//le token est désactivé car .disable()
                .csrf(csrf -> csrf.disable())
                // on affiche le formulaire d'authentification si c est réussi redirect page acceuil si non re authentificqtion
                .formLogin(form -> form
                        .loginPage("/login").defaultSuccessUrl("/etudiant/acceuil").failureUrl("/login")
                        .permitAll()  //j'authorise tous le monde permitAll()
                        //je clique sur la page équipe il me demande de s'uathentifié je m'authentifie à l'aide de la méthode defaultSuccessUrl
                        // apres l'authentification je me dérige vers la page équipe et sans cette méthode il me dérife vers la deriére page visité
                        //avant mon 1 clique
                        // defaultSuccessUrl pour éviter de me rédiriger vers la derniérer page avant de demander la page  mais vers l'url de demandé
                )
                // route logout boutton thymeleaf
                .logout(logout-> logout
                        .logoutUrl("/logout")
                        .permitAll()
                )
                //dire à spring security toute les reqûette nécessite
                // une authentification.anyRequest().authentificated()
                //personnaliser la sécurité au niveau des requette
                .authorizeHttpRequests(auth -> auth
                        //capturer quel requette
                        .requestMatchers("/reset-password/**","/forgot-password","/acceuil","/inscription","/css/**", "/js/**", "/img/**", "/favicon.ico","/webjars/**").permitAll()
                        //.requestMatchers(HttpMethod.POST,("/inscription")).permitAll()
                        //Interdit la page si l'utilisateur n'est pas admin
                        .requestMatchers("/admin/**").hasRole("ADMIN") //j'authorise que la route qui commence par  /admin d'y acceder à ces page
                            .requestMatchers("/etudiant/**").hasAnyRole("ADMIN","ETUDIANT","TUTEUR")
                        .requestMatchers("/tuteur/**").hasAnyRole("ADMIN","TUTEUR")
                        .anyRequest().authenticated()
                )
                .build();
    }

}


