package com.example.pfe.DAO;

import com.example.pfe.Model.Rentree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface RentreeDAO extends JpaRepository<Rentree, Long> {
   Page<Rentree> findByIdContainingIgnoreCase(String key, Pageable pageable);




}










