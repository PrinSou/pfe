package com.example.pfe.DAO;

import com.example.pfe.Model.Projet;
import jakarta.validation.constraints.NotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository

public interface ProjetDAO extends JpaRepository<Projet, Long> {
    Page<Projet> findByNomContainingIgnoreCase(String key,Pageable page);

    @Override
    Optional<Projet> findById(Long id);



    /**   List<Projet> findByNomIgnoreCase(String nom);



    /***
     * le paramétre Pageable sert pour la pagination c à dire je peux transmettre le numéro
     * de la page et le size5NBR D4AFFICHAGE
     *
     */


    // Cette requête cherche dans la liste des équipes de chaque projet
    // celle qui n'est pas complète (isComplet est false)
    @Query("SELECT p FROM Projet p JOIN p.equipeList equipe WHERE size(equipe.etudiantList) < 3")
    List<Projet> findAllByEquipeListNonCompletes();



}


