package com.example.pfe.DAO;

import com.example.pfe.Model.Professeur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface ProfesseurDAO extends JpaRepository<Professeur, Long> {
    Page<Professeur> findByNomContainsIgnoreCase(String key, Pageable pageable);


    @Override
    Optional<Professeur> findById(Long aLong);
}
