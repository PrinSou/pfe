package com.example.pfe.DAO;

import com.example.pfe.Model.JetonResetMdp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface JetonResetMdpDAO extends JpaRepository<JetonResetMdp, Long> {
    JetonResetMdp findByToken(String token);


}
