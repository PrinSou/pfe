package com.example.pfe.DAO;

import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.JetonResetMdp;
import com.example.pfe.Model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Le UserRepository s'étend du CrudRepository. Il fournit le type de l'entité (Long) et de sa clé primaire(User).**/

@Repository

public interface UtilisateurDAO extends JpaRepository<Utilisateur,Long>{
 List<Utilisateur> findByNom(String nomUtilisateur);

 Utilisateur findByEmail(String email);

 Optional<Utilisateur> findByJetonResetMdps_TokenIgnoreCase(String token);

 @Transactional
 @Modifying
 @Query("update Utilisateur u set u.mdp = ?1 where u.id = ?2")
 int updateMdpById(String mdp, Long id);















}
/**
 * Comme à notre habitude, expliquons : @Repository est une annotation Spring pour indiquer
 * que la classe est un bean,
 * et que son rôle est de communiquer avec une source de données (en l'occurrence la base de données).
 */

/***
 * un bean c est un bloc magique dans mon jeu de bloc il
 *
 * pourra se communiquer avec d'autre boite
 * magique et elles s'entraident entre eux pour faire ce qu'on leur demande
 */