package com.example.pfe.DAO;
import com.example.pfe.Model.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AdminDAO extends JpaRepository<Admin, Long> {
    Page<Admin> findByNomContainingIgnoreCase(String nom, Pageable pageable);



}
