package com.example.pfe.DAO;

import com.example.pfe.Model.Etudiant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EtudiantDAO extends JpaRepository<Etudiant, Long> {
    Page<Etudiant> findByNomContainingIgnoreCase(String key, Pageable page);

    Optional<Etudiant> findByEmail(String email);



}
