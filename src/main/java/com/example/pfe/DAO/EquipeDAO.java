package com.example.pfe.DAO;

import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Projet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EquipeDAO extends JpaRepository<Equipe,Long> {
    @Query("select e from Equipe e where e.projet = ?1")
    List<Equipe> findEquipeByProjet(Projet projet);
    Page<Equipe> findByNomContainsIgnoreCase(String key, Pageable pageable);

    List<Equipe> getEquipeByNom(String nom);



    // Utilisation de JPQL pour compter le nombre d'étudiants dans les équipes et les filtrer
    @Query("SELECT e FROM Equipe e WHERE e.projet = :projet AND SIZE(e.etudiantList) < :maxSize")
    List<Equipe> findByProjetAndEtudiantListSizeLessThan(@Param("projet") Projet projet, @Param("maxSize") int size);

    //moins que
  //  List<Equipe>findByCandidatListLessThan(int number);



    Optional<Equipe> findByProjetId(Long idProjet);

}
