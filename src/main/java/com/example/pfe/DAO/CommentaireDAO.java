package com.example.pfe.DAO;

import com.example.pfe.Model.Commentaire;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CommentaireDAO extends JpaRepository<Commentaire, Long> {
    Page<Commentaire> findByNoteContainingIgnoreCase(String key, Pageable pageable);


}
