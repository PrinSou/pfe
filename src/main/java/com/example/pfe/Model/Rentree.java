
package com.example.pfe.Model;

import jakarta.persistence.*;


import java.sql.Date;

import java.util.LinkedList;
import java.util.List;

@Entity
public class Rentree {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private Date annee;

    @ManyToOne
    @JoinColumn(name = "idAdmin")
    private Admin admin;

    @ManyToMany
    @JoinTable(name = "etudiant_rentree",joinColumns = @JoinColumn(name = "idRentree"),
            inverseJoinColumns = @JoinColumn(name ="idEtudiant" ))
    private List<Etudiant> etudiantList =new LinkedList<>();

    @OneToMany(mappedBy = "rentree", cascade = CascadeType.ALL)
    private List<Projet>ProjetList;

    @ManyToMany
    @JoinTable(name= "professeur_rentree", joinColumns = @JoinColumn(name="idRentree"),
            inverseJoinColumns = @JoinColumn(name = "idProfesseur"))
    private List<Professeur>professeurList;
    public Rentree(Long id, Date annee) {
        this.id = id;
        this.annee = annee;
    }


    public Rentree() {
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rentree rentree)) return false;

        if (!getId().equals(rentree.getId())) return false;
        return getAnnee().equals(rentree.getAnnee());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getAnnee().hashCode();
        return result;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Date getAnnee() {
        return annee;
    }

    public void setAnnee(Date annee) {
        this.annee = annee;
    }

    public List<Projet> getProjetList() {
        return ProjetList;
    }

    public void setProjetList(List<Projet> projetList) {
        ProjetList = projetList;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public List<Etudiant> getEtudiantList() {
        return etudiantList;
    }

    public void setEtudiantList(List<Etudiant> etudiantList) {
        this.etudiantList = etudiantList;
    }

    public List<Professeur> getProfesseurList() {
        return professeurList;
    }

    public void setProfesseurList(List<Professeur> professeurList) {
        this.professeurList = professeurList;
    }
}
