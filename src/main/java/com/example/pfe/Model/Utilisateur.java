
package com.example.pfe.Model;

import jakarta.persistence.*;

import javax.management.relation.Role;
import java.util.ArrayList;
import java.util.List;

//@PasswordCorres
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_utilisateur")
@Table(name = "utilisateur")
public abstract class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Column(name = "email")
    private String email;

    @Column(name = "mdp")
    private String mdp;

    @Column(name="confirmMdp")
    private String confirmMdp;

    @Column(name = "role")
    private String role;

    public Utilisateur() {
    }

    public Utilisateur(Long id, String prenom, String nom, String email, String mdp, String confirmMdp) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.mdp = mdp;
        this.confirmMdp=confirmMdp;
    }

    @OneToMany(mappedBy = "utilisateur",cascade = CascadeType.ALL)
    private List<Commentaire>commentaireList;


    @OneToMany(mappedBy = "utilisateur", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<JetonResetMdp> jetonResetMdps = new ArrayList<>();
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getConfirmMdp() {
        return confirmMdp;
    }

    public void setConfirmMdp(String confirmMdp) {
        this.confirmMdp = confirmMdp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public List<JetonResetMdp> getJetonResetMdps() {
        return jetonResetMdps;
    }

    public void setJetonResetMdps(List<JetonResetMdp> jetonResetMdps) {
        this.jetonResetMdps = jetonResetMdps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilisateur that)) return false;

        if (!getId().equals(that.getId())) return false;
        if (!getPrenom().equals(that.getPrenom())) return false;
        if (!getNom().equals(that.getNom())) return false;
        if (!getEmail().equals(that.getEmail())) return false;
        if (!getMdp().equals(that.getMdp())) return false;
        if (!getConfirmMdp().equals(that.getConfirmMdp())) return false;
        if (!getRole().equals(that.getRole())) return false;
        return getCommentaireList() != null ? getCommentaireList().equals(that.getCommentaireList()) : that.getCommentaireList() == null;
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getPrenom().hashCode();
        result = 31 * result + getNom().hashCode();
        result = 31 * result + getEmail().hashCode();
        result = 31 * result + getMdp().hashCode();
        result = 31 * result + getConfirmMdp().hashCode();
        result = 31 * result + getRole().hashCode();
        result = 31 * result + (getCommentaireList() != null ? getCommentaireList().hashCode() : 0);
        return result;
    }

    public List<Commentaire> getCommentaireList() {
        return commentaireList;
    }

    public void setCommentaireList(List<Commentaire> commentaireList) {
        this.commentaireList = commentaireList;
    }
}