package com.example.pfe.Model;
import jakarta.persistence.*;

import java.util.List;
@Entity
@DiscriminatorValue("admin")
public class Admin extends Utilisateur{
  private String role;
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    @OneToMany(mappedBy = "admin",cascade = CascadeType.ALL)
    private List<Rentree> rentreeList;
    public Admin() {
    }
    public Admin(Long id, String nom, String prenom, String email, String mdp, String confirmMdp){
        super(id,prenom,nom,email,mdp,confirmMdp);
    }
    public List<Rentree> getRentreeList() {
        return rentreeList;
    }
    public void setRentreeList(List<Rentree> rentreeList) {
        this.rentreeList = rentreeList;
    }
}
