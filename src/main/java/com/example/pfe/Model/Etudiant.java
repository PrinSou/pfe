package com.example.pfe.Model;

import jakarta.persistence.*;

import javax.management.relation.Role;
import java.util.LinkedList;
import java.util.List;

@Entity
@DiscriminatorValue(value="etudiant")
public class Etudiant extends Utilisateur {



    @ManyToMany
    @JoinTable(name= "etudiant_rentree", joinColumns = @JoinColumn(name="idEtudiant"),
            inverseJoinColumns = @JoinColumn(name = "idRentree"))
    private List<Rentree>rentreeList;

    @ManyToOne
    @JoinColumn(name="idEquipe")
    private  Equipe equipe;

    public Etudiant(Long id, String nom, String prenom, String email, String mdp, String confirmMdp){
        super(id,prenom,nom,email,mdp,confirmMdp);

    }

    public Etudiant() {
        super();
    }



    public List<Rentree> getRentreeList() {
        return rentreeList;
    }

    public void setRentreeList(List<Rentree> rentreeList) {
        this.rentreeList = rentreeList;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }





}

