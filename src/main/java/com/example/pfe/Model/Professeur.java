package com.example.pfe.Model;
import jakarta.persistence.*;

import java.util.List;
@Entity
@DiscriminatorValue("tuteur")
public class Professeur extends Utilisateur {
   private String role;
    @ManyToMany
    @JoinTable(name= "professeur_rentree", joinColumns = @JoinColumn(name="idProfesseur"),
            inverseJoinColumns = @JoinColumn(name = "idRentree"))
    private List<Rentree>rentreeList;
    @OneToMany(mappedBy = "professeur",cascade = CascadeType.ALL)
    private List<Projet> projetList;
    public Professeur() {
    }
    public Professeur(Long id, String nom, String prenom, String email, String mdp, String confirmMdp) {
        super(id, prenom, nom, email, mdp, confirmMdp);
    }
    @Override
    public String getRole() {
        return role;
    }
    @Override
    public void setRole(String role) {
        this.role = role;
    }

    public List<Projet> getProjetList() {
        return projetList;
    }

    public void setProjetList(List<Projet> projetList) {
        this.projetList = projetList;
    }

    public List<Rentree> getRentreeList() {
        return rentreeList;
    }

    public void setRentreeList(List<Rentree> rentreeList) {
        this.rentreeList = rentreeList;
    }


}
