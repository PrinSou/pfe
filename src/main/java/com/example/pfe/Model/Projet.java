
package com.example.pfe.Model;

import jakarta.persistence.*;


import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity

/**
 * une entity doit avoir un identifiant et pour que la valeur sois generer authomatiquement on utulise:@GeneratedValue(strategy = GenerationType.IDENTITY)
 * npusa vons créer une entity JPA
 */
public class Projet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
   // @Column(unique = true,nullable = false)
    private String nom;

    /**@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)**/
    private Date dateDebut;

    /**@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)**/
    private Date dateFin;



    @ManyToOne
    @JoinColumn(name = "idRentree")
    private Rentree rentree;

    @OneToMany(mappedBy = "projet", cascade = CascadeType.ALL)
    private List<Equipe> equipeList;

    @ManyToOne
    @JoinColumn(name="idProfesseur")
    private  Professeur professeur;

    public Projet(Long id, String nom, Date dateDebut, Date dateFin) {
        this.id = id;
        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Projet projet = (Projet) o;

        if (!Objects.equals(id, projet.id)) return false;
        if (!nom.equals(projet.nom)) return false;
        if (!dateDebut.equals(projet.dateDebut)) return false;
        return dateFin.equals(projet.dateFin);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + nom.hashCode();
        result = 31 * result + dateDebut.hashCode();
        result = 31 * result + dateFin.hashCode();
        return result;
    }

    public Projet() {
    }

    public Rentree getRentree() {
        return rentree;
    }

    public void setRentree(Rentree rentree) {
        this.rentree = rentree;
    }

    /**
     * faut récupérer les getteur et setteur de la session apres l'association
     * idem pour le session faut recuperer les getteur et setteur du projet
     * @return
     */


    public List<Equipe> getEquipeList() {
        return equipeList;
    }

    public void setEquipeList(List<Equipe> equipeList) {
        this.equipeList = equipeList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Professeur getProfesseur() {
        return professeur;
    }

    public void setProfesseur(Professeur professeur) {
        this.professeur = professeur;
    }

    public Boolean isOpen(){ return this.equipeList.stream().anyMatch(e->e.isComplet()==false);}
}
//je recupere tous les projets de l'anne en cours qu'ils soient open