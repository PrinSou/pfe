
package com.example.pfe.Model;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Equipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String nom;

    @ManyToOne
    @JoinColumn(name = "idProjet")
    private Projet projet;

    @OneToMany(mappedBy = "equipe", cascade = CascadeType.ALL)
    private List<Etudiant> etudiantList;

    @OneToMany(mappedBy = "equipe", cascade = CascadeType.ALL)
    private List<Commentaire>commentaireList;

    public Equipe(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Equipe() {
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Equipe equipe = (Equipe) o;

        if (!Objects.equals(id, equipe.id)) return false;
        return Objects.equals(nom, equipe.nom);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean isComplet() {
        // Retourne true si la taille de la liste des étudiants est de 3 ou plus
        return this.etudiantList.size() >= 3;
    }
    public void addEtudiant(Etudiant etudiant) {
        // Vous pouvez également vérifier si l'équipe est complète avant d'ajouter un étudiant
        if (!isComplet()) {
            this.etudiantList.add(etudiant);
            etudiant.setEquipe(this);
        } else {
            throw new IllegalStateException("L'équipe a atteint sa capacité maximale.");
        }
    }

    // Autres méthodes...

 //   Avec cette méthode estComplete, chaque fois que vous tentez d'ajouter un étudiant à une équipe, vous pouvez vérifier si l'équipe a de la place disponible. La méthode addEtudiant intègre cette logique pour éviter d'ajouter un étudiant à une équipe complète.







    public List<Etudiant> getEtudiantList() {
        return etudiantList;
    }
    public void setEtudiantList(List<Etudiant> etudiantList) {
        this.etudiantList = etudiantList;
    }

    public List<Commentaire> getCommentaireList() {
        return commentaireList;
    }

    public void setCommentaireList(List<Commentaire> commentaireList) {
        this.commentaireList = commentaireList;
    }
}
