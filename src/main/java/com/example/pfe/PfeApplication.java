package com.example.pfe;
import com.example.pfe.Service.EtudiantService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

/**quand on utulise l'annotaion Bean c à dire que spring va appellé cette methode lors de l'exécution au démarrage

/**
 * tester ou faire un traitement au démarrage
 * faut implementer l'interface CommandeLineRunner er redefinir la méthode run
 * où nous allons éxcuter un traitement au démarrage donc nous allons besoin d'injecter
 * les dépondance avec @Autowired
 * et 'utuliser l'interface DAO
 * et créer nos objet
 * il y' 3 façon pour créer un objet avec un constructeur sans paramétre et  un constructeur avec paramétre
 * et avec @Builder si on traveille avec ma dé^pondance @Lombok
 *Avec Spring Data on peux ajouter des données dans la base de donnée on utulisant les constructeur
 */
@SpringBootApplication
@Builder
@AllArgsConstructor
public class PfeApplication {
    /*PasswordEncoder passwordEncoder;

    @Autowired
    private static EtudiantService etudiantService;*/

    public static void main(String[] args) {
        SpringApplication.run(PfeApplication.class, args);

       /* // Appeler la méthode assignerEtudiantsAuxEquipes()
        etudiantService.assignerEtudiantsAuxEquipes();

        // Afficher un message de confirmation
        System.out.println("Les étudiants ont été affectés aux équipes.");*/
    }
}

 /*   @Bean
   public CommandLineRunner commandLineRunner() {
        return args -> {
            Candidat candidat = new Candidat(null, "ti", "ines", "ti.ines@gmail.com", "12345678","12345678");


            candidatService.inscrireCandidat(candidatService.toCandidatDTO(candidat));

        };
    }*/



/*
    @Configuration
    public class PdfConfiguration {

        @Bean
        public Exporter pdfExporter() {
            return new PdfExporter();
        }

        @Bean
        public Importer pdfImporter() {
            return new PdfImporter();
        }

    }

 */

/**
public class PfeApplication implements CommandLineRunner {
    private final ProjetDAO projetDAO;

    public PfeApplication(ProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    public static void main(String[] args) {
        SpringApplication.run(PfeApplication.class, args);
 la méthode run lance l'application spring
    }


    @Override
    public void run(String... args) throws Exception {

        /**
         * pour enregistrer les projets on utulise le DAO
         */
       /** projetDAO.save(new Projet(null,"Banking",new Date(),new Date()));
        projetDAO.save(new Projet(null,"E-Commerce",new Date(),new Date()));
        projetDAO.save(new Projet(null,"PlanExamx",new Date(),new Date()));
/**
 * un constructeur sans paramétre: on cré le constructeur et on récupere les setteur

        Projet projet=new Projet();
        projet.setId(null);
        projet.setNom("Banking");
        projet.setDateDebut(new Date());
        projet.setDateFin(new Date());


**/
/**
 * un constructeur avec paramétre
 *
        Projet projet =new Projet(null,"Banking",new Date(),new Date());
        **/


    /**
     * ou bien utuliser Builder avec l'indépondance @Lombok

    Projet projet=Projet.builder()
            .nom("Banking")
            .dateDebut(new Date())
            .dateFin(new  Date())
            .build();
     **/

