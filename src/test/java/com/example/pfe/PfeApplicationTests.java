package com.example.pfe;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PfeApplicationTests {

    /***
     * elle permet de tester des methode faite par moi même  et des DAO
     *
     */
        @Autowired
        private UserDetailsService userDetailsService;

        @Test
        public void testAuthentification() {
            // Authentifie un utilisateur avec le rôle ROLE_TUTEUR
            UserDetails userDetails = userDetailsService.loadUserByUsername("test@example.com");

            // Vérifie que l'utilisateur a le rôle ROLE_TUTEUR
            assertTrue(userDetails.getAuthorities().stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_TUTEUR")));
        }

    @Test
    void contextLoads() {
    }

}
